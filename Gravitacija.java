import java.util.Scanner;
public class Gravitacija {
    
    public static double izracun(double visina){
        return 6.674*Math.pow(10,-11) * 5.972 * Math.pow(10,24)/Math.pow((6.371*Math.pow(10,6)+visina),2);
    }
    public static void main (String [] args ) {
        Scanner sc = new Scanner(System.in);
        System.out.println("OIS je zakon!");
       
        System.out.print("Vnesi nadmorsko visino: ");
        int nadmorskaVisina = sc.nextInt();
        System.out.print("\n");
        
        izpis(izracun(nadmorskaVisina), nadmorskaVisina);
    }
    
    public static void izpis(double pospesek, double visina)
    {
        System.out.println(visina);
        System.out.println(pospesek);
    }
}